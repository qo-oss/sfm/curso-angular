import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'sfm-ejercicios';

  miNumero: number = 45;

  constructor() {
    setInterval(() => {
      this.miNumero = Math.floor(Math.random() * 100);
    }, 2000);
  }

  recibirFecha(fecha: string) {
    console.log(`Fecha recibida: ${fecha}`);
    // Fecha recibida: 2022-06-05
  }
}
