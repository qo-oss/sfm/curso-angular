import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ej1',
  templateUrl: './ej1.component.html',
  styleUrls: ['./ej1.component.scss']
})
export class Ej1Component implements OnInit {

  private _numero: number = 0;

  @Input('numero')
  set numero(num: number) {
    this._numero = (num || 0);
    this.result = this._numero * 3;
  }
  get numero(): number {
    return this._numero;
  }

  result?: number;

  constructor() { }

  ngOnInit(): void {
    
  }

}
