import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateTime } from 'luxon';

@Component({
  selector: 'fecha-futura',
  templateUrl: './fecha-futura.component.html',
  styleUrls: ['./fecha-futura.component.scss']
})
export class FechaFuturaComponent implements OnInit {

  formDias: FormGroup = this.fb.group({
    dias: [null, Validators.required]
  });

  @Output('nuevaFecha')
  nuevaFecha: EventEmitter<string> = new EventEmitter<string>();

  fechaFutura?: Date;
  

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    
  }

  sumar() : void {
    console.log(this.formDias.value);
    const today = new Date();
    const dias = this.formDias.get('dias')?.value;
    //this.fechaFutura = new Date(today.getTime() + dias*24*60*60*1000); 
    this.fechaFutura = DateTime.now().plus({days: dias}).toJSDate();
    const fechaIso :string = DateTime.fromJSDate(this.fechaFutura).toISODate();
    this.nuevaFecha.emit(fechaIso);
    // Fecha futura: Sat May 07 2022 13:29:26 GMT+0200 (Central European Summer Time)
    // Fecha futura: 05/05/2022
  }

}
