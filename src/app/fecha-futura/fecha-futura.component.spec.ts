import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FechaFuturaComponent } from './fecha-futura.component';

describe('FechaFuturaComponent', () => {
  let component: FechaFuturaComponent;
  let fixture: ComponentFixture<FechaFuturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FechaFuturaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FechaFuturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
