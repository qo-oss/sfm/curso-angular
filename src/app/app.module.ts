import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Ej1Component } from './ej1/ej1.component';
import { FechaFuturaComponent } from './fecha-futura/fecha-futura.component';

@NgModule({
  declarations: [
    AppComponent,
    Ej1Component,
    FechaFuturaComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,    
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
