## Ejercicio 1

Creación de un componente Angular, llamado ej1 que muestre un número que recibe por parámetro (@Input('numero')) y tb. el resultado de multiplicar por 3 dicho número.

Podéis utilizar la guía con los contenidos de los videos creada en la formación realizada de Angular donde se enumera los temas tratados en cada video, varios de esos conceptos se aplicarán en el presente ejercicio. 
Contenidos: https://gitlab.com/qo-oss/sfm-demo-app/-/blob/master/VIDEOS.md

Para probar el componente ej1, lo usaremos en el componente app.componentque es creado en todas las aplicaciones de Angular, el número que se utilizará como parámetro de entrada de ej1 será almacenado en un atributo del AppComponent

Cuando el valor del input cambie, el componente debe actualizar automáticamente el resultado y mostrarlo en pantalla. Se sugiere el uso de "getters" y "setters" en Typescript, para que mediante el "setter" se detecte cuando un nuevo valor llega y se opere en consecuencia.

Visualmente el número original debe aparecer:

   * En gris (RGB: `#cccccc`)
   * Tamaño de fuente: `0.8rem` (`font-size: 0.8rem`)

El número resultado de multiplicar por 3 debe parecer:

   * Debajo del número original con una pequeña separación
   * En color azul (vale cualquier tono de azul)
   * Con un tamaño 1.4rem  (`font-size: 1.4rem`)
   * En negrita, usando el estilo css `font-weight`

## Ejercicio 2

Creación de un componente en angular que se llame fecha-futura dicho componente sumará días a la fecha actual del sistema y mostrará la fecha resultante en pantalla. Tendrá un parámetro de salida (`@Output`) llamado `nuevaFecha` para notificar al componente padre cuando la fecha es calculada.

Podéis utilizar la guía con los contenidos de los videos creada en la formación realizada de Angular donde se enumera los temas tratados en cada video, varios de esos conceptos se aplicarán en el presente ejercicio. 
Contenidos: https://gitlab.com/qo-oss/sfm-demo-app/-/blob/master/VIDEOS.md

Requisitos:

* Utilizar la librería de Angular @angular/flex-layout para organizar el layout del componente. Será necesario añadir la dependencia al proyecto y el módulo correspondiente en la sección imports del módulo principal de la aplicación.

* Se creará un `<input>` en el html donde se introducirá el número de días, el número puede ser negativo, debe aparecer un label sobre el `<input>` que indique "Días futuros"
* Se debe utilizar un formulario reactivo de Angular para recuperar los datos del input (Angular Reactive Form). Añadir el módulo correspondiente de Angular que corresponda.
* Se añadirá un botón con el texto [Sumar] a la derecha del input que cuando se pulse realice la suma de días a la fecha actual. Para realizar el cálculo podemos utilizar el objeto Date de JavaScript o bien una librería externa como luxon Método [`DateTime.plus()`](https://moment.github.io/luxon/api-docs/index.html#datetimeplus). 
Cuando se pulse la tecla [Enter] al estar editando el número se realizará la suma de días como se se hubiera pulsado en el botón.
* Al calcular la nueva fecha se emitirá un evento para el `@Output` con la nueva fecha, la notificación será de tipo string con el formato ISO: `YYYY-MM-DD`.  Añadir un handler en el componente padre para mostrar por consola (console.log) el valor de al fecha emitida desde fecha-futura
* La fecha resultante se mostrará en pantalla, debajo del input y el botón y con el siguiente aspecto:
     * Tamaño de fuente: `1.4rem`
     * En negrita
     * Color variable en función de si los días sumados son positivos (mostrar en verde) o negativos (mostrar en rojo).
     * En formato: `20/03/2022`, usar el pipe date de Angular para aplicar el formato


## Ejercicio 3

Desarrollaremos un componente que muestren un listado de ciudades, para ello vamos a invocar una servicio REST y mostraremos los datos formateados en pantalla.

Podéis utilizar la guía con los contenidos de los videos creada en la formación realizada de Angular donde se enumera los temas tratados en cada video, varios de esos conceptos se aplicarán en el presente ejercicio. 
Contenidos: https://gitlab.com/qo-oss/sfm-demo-app/-/blob/master/VIDEOS.md

Requisitos:

* Añadiremos el módulo `HttpClientModule` a nuestro proyecto, importándolo desde:

```typescript
import { HttpClientModule } from '@angular/common/http';
```
Esto nos permitirá utilizar la clase `HttpClient`en nuestro código.

* Para recuperar datos desde un back vamos a utilizar el mismo programa utilizado en el Curso de Angular (podéis ver cómo usarlo en la sesión 3-1, min: 3:30 aprox) que proporcionará un API REST sencillo para nuestro ejercicio. Dicho servidor lo podéis encontrar en el directorio "server" del repositorio del proyecto utilizado para dicho curso, exactamente en: https://gitlab.com/qo-oss/sfm-demo-app/-/tree/master/server
Lo más sencillo será copiar el directorio "server" completo a nuestro proyecto y levantar el servidor tal y como se indica en la documentación "server/README.md".
Una vez levantado el servidor deberá escuchar peticiones en la url: `http://localhost:3000`

* Para recuperar los datos de este back vamos a crear un modelo de datos para hacer el mapeo de la información que nos va a llegar, para ello crearemos una carpeta en "src/app/model" Y dentro vamos a crear un fichero llamado `model.ts`
En este fichero vamos a definir el formato que tiene tanto una ciudad como un país:
```typescript
export interface Pais {
    code: string;
    name: string;
}
export interface Ciudad {
    id?: number;
    name: string;
    country_code: string;
    population: number;
    country: Pais;
}
```
* Crearemos un Service en Angular que será el encargado de realizar la petición http, para ello usaremos el comando ya conocido:
```
npx ng generate services/ciudad
```
Esto nos debe haber generado un nuevo service ubicado en `app/services/ciudad.service.ts`
* Inyectaremos en el nuevo service una instancia de HttpClient. El cómo hacer la inyección del Httpclient lo podéis ver en el video correspondiente del primer curso (sesión 3-1, min: 8 aprox) y en la ayuda del servidor REST en [server/README.md](https://gitlab.com/qo-oss/sfm-demo-app/-/tree/master/server/README.md)
* Crearemos en `ciudad.service.ts` un método llamado `listaCiudades()` que retorne todas las ciudades mediante la llamada al servicio REST: `/api/cities` El servicio REST retornará una lista de ciudades ( `Ciudad[]`) Utilizaremos el objeto `HttpClient` inyectado en la clase para hacer la invocación, ver ejemplos en el curso anterior o en la propia ayuda del servidor REST. El nuevo método debe retornar lo mismo que el método utilizado de `Httpclient`, es decir, un `Observable` y en este caso que el dato devuelto por el Observable sea un `Ciudad[]`, ver los ejemplos citados para casos similares.
NOTA: Para montar la url utilizaremos una URL absoluta, con la ubicación del servidor, mencionada más arriba, y la ruta del servicio REST a llamar.
NOTA2: Una vez que funcione este apartado, se podría cambiar para usar el `environment` de Angular para almacenar la ruta del servidor. Es decir, se podría crear un parámetro `server_url_base` con la url base del servidor, `http://localhost:3000/api`, podéis ver cómo se configura en la sesión del curso 3-1 (min 9:30 aprox). 

* Antes de continuar con el resto de componentes del ejercicio vamos a añadir al proyecto `@angular/material`, podéis seguir la guia oficial en: https://material.angular.io/guide/getting-started O revisar el video del curso, sesión 2-1 (min 48 aprox). No es imprescindible, pero podemos añadir a nuestra aplicación un nuevo módulo que importe la mayor parte de los componentes de Angular Material, tal y como se ve en esa misma sesión del curso, el módulo lo teneis en la url: https://gitlab.com/qo-oss/sfm-demo-app/-/blob/master/src/app/my-material/my-material.module.ts Otra opción es importar directamente en el app.module aquellos componentes (módulos) de Material que vamos a utilizar.

* A continuación crearemos un nuevo componente (listado-ciudades) que muestre la lista de ciudades en pantalla, como primer paso crearemos el componente con el comando `npx ng generate ...` y luego haremos el componente accesible mediante una ruta, haciendo uso de Routing de Angular (`app-routing.module.ts`). Crearemos una ruta `"ciudades"` que muestre el nuevo componente, para ello vamos a necesitar otro componente al que vamos a llamar "home", que crearemos de la misma manera y que vamos a asociar a la ruta raiz `""`, el contenido de este componente "home" será el que ahora aparece en el app.component (tanto la parte html como la ts), y el componente app.component pasará a contener un simple tag: `<router-outlet></router-outlet>`, podemos ver cómo crear las rutas en al sesión 2-1 del curso (min 21:30 aprox)
Una vez aplicadas las rutas, al acceder a la url "http://localhost:4200/ciudades" Debemos ver el contenido del nuevo componente `listado-ciudades` De momento vacío, o con el mensaje por defecto de los nuevos componentes en Angular.

* Para mostrar la lista de ciudades en el nuevo componente vamos a añadir el servicio creado antes (`CiudadService`) como elemento inyectado en el constructor. podéis ver un caso similar en la sesión 2-2 del curso (min: 40:20 aprox). Añadiremos un atributo (`ciudades`) de tipo `Ciudad[]` en el componente que recoja los datos recuperados del Observable que retorna el método `listaCiudades()`, en la misma sesión del curso podéis ver cómo recuperar datos de un Observable (min 44 aprox).

* Una vez recuperada la lista de ciudades vamos a mostrarla en pantalla, para ello haremos uso de algún componente de Angular Material, entre las opciones más habituales tenemos (hay que elegir una de ellas):
  * `MatTable`: https://material.angular.io/components/table/overview
  * `MatList`: https://material.angular.io/components/list/overview
  
En ambos casos debemos mostrar el Nombre de la ciudad, el nombre del país al que pertenece y el número de habitantes. Cómo utilizar una tabla lo tenéis en el curso, en la sesión 3-1 (min 37 aprox). En el caso del MatList no existen las columnas por lo que el layout del contenido quedaría a nuestra discreción, podemos mostrar el nombre del ciudad, debajo el nombre del país y a la derecha el número de habitantes entre paréntesis (por ejemplo), para la organización del layout es recomendable el uso de flex-layout, sesión 1-2 (min: 54 aprox). Por ejemplo:
```
+-----------------------------+
| Badajoz       (hab: 100000) |
| España                      |
+-----------------------------+
| Paris        (hab: 8000000) |
| Francia                     |
+-----------------------------+
```

Sería interesante que una vez funcione con una de las opciones se intente el desarrollo de la otra opción para ver las diferencias.

