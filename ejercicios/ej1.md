### Ejercicio 1

Creación de un componente Angular, llamado `ej1` que muestre un número que recibe por parámetro y tb. el resultado de multiplicar por 3 dicho número.

Para probar el componente ej1, lo usaremos en el componente app.componentque es creado en todas las aplicaciones de Angular, el número que se utilizará como parámetro de entrada de ej1 será almacenado en un atributo del AppComponent

Visualmente el número original debe aparecer:
   * En gris (RGB: `#cccccc`)
   * Tamaño de fuente: 0.8rem (`font-size: 0.8rem`)

El número resultado de multiplicar por 3 debe parecer:
   * Debajo del número original con una pequeña separación
   * En color azul (vale cualquier tono de azul)
   * Con un tamaño 1.4rem  (font-size: 1.4rem)
   * En negrita, usando el estilo css font-weight